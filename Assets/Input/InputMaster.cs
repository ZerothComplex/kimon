// GENERATED AUTOMATICALLY FROM 'Assets/Input/InputMaster.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class InputMaster : IInputActionCollection, IDisposable
{
    private InputActionAsset asset;
    public InputMaster()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputMaster"",
    ""maps"": [
        {
            ""name"": ""Hero"",
            ""id"": ""11f46eec-d5a8-4c4f-9a2e-13138fdf3b5f"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Button"",
                    ""id"": ""09016560-6dae-493f-94c8-9c0d99a2f208"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""BasicAttack"",
                    ""type"": ""Button"",
                    ""id"": ""ea82e9dd-344f-4bb1-83b2-5ae6de148940"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Cleave"",
                    ""type"": ""Button"",
                    ""id"": ""666562e0-5135-4c40-a830-abbcc6abdffc"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Dash"",
                    ""type"": ""Button"",
                    ""id"": ""962da32d-1cfa-4715-9d0b-375da288d49b"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""wasdKeys"",
                    ""id"": ""7d3a325a-b86b-4d0c-aac4-509b31821329"",
                    ""path"": ""2DVector"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""8dd1928e-d3da-4deb-9e91-7ef27475781e"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""4a8ff251-ec72-4ec6-bd70-e73db4ac4b76"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""ff20f17e-b869-4e00-be3b-c0bbb70fdc9e"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""218edf33-ce91-4fb9-b428-db9b1cf312af"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""45a0d4b9-0b8f-47a2-a868-31ecd58a5ea9"",
                    ""path"": ""<Keyboard>/e"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""BasicAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""7ce09371-1050-45ac-b0c9-f204ad39d25f"",
                    ""path"": ""<Keyboard>/f"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Cleave"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f37f6155-f408-4816-9acb-1eca52cbc43c"",
                    ""path"": ""<Keyboard>/q"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Dash"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Monster"",
            ""id"": ""b48435ef-13a3-4559-8422-fc3f603d3414"",
            ""actions"": [
                {
                    ""name"": ""Movement"",
                    ""type"": ""Button"",
                    ""id"": ""604d5799-f5d1-41a3-bbaa-c8a388a5e658"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""BasicAttack"",
                    ""type"": ""Button"",
                    ""id"": ""5066bf26-b4d1-4907-bd1a-076e759be035"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fireball"",
                    ""type"": ""Button"",
                    ""id"": ""52389ed5-f342-49f2-836a-547f82592434"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Explosion"",
                    ""type"": ""Button"",
                    ""id"": ""01c40c56-b8ae-4ea7-a036-3037c54231ea"",
                    ""expectedControlType"": """",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""arrowKeys"",
                    ""id"": ""dcb9ddca-0ae7-4544-aa23-5e9a4eb89e25"",
                    ""path"": ""2DVector"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Movement"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""01f0127e-0c3a-4cb8-840d-777beeb3bea5"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""da77f533-5bbb-4f09-a503-d12dfbab654b"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""661d4d17-f6f8-45e6-9a29-33023166a049"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""7083b22b-cc85-4c13-8af7-e13de3e32280"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Movement"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""83cf5428-0377-4d25-9626-78dc8ae031a3"",
                    ""path"": ""<Keyboard>/j"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""BasicAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""6f1f58e6-595e-40ac-8202-b1a58bcfdf1f"",
                    ""path"": ""<Keyboard>/numpad1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""BasicAttack"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""57de37ac-dc55-4132-af7d-47e202faf529"",
                    ""path"": ""<Keyboard>/k"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Fireball"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0f55138c-1615-4c74-836d-668b0fe0d69d"",
                    ""path"": ""<Keyboard>/numpad2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Fireball"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""4a0e5b63-62be-4c8a-b07c-53eb8dd37c70"",
                    ""path"": ""<Keyboard>/l"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Explosion"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""ac722b27-bfd3-4028-a0b0-4d04678b3aed"",
                    ""path"": ""<Keyboard>/numpad3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": ""Keyboard"",
                    ""action"": ""Explosion"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": [
        {
            ""name"": ""Keyboard"",
            ""bindingGroup"": ""Keyboard"",
            ""devices"": [
                {
                    ""devicePath"": ""<Keyboard>"",
                    ""isOptional"": false,
                    ""isOR"": false
                }
            ]
        }
    ]
}");
        // Hero
        m_Hero = asset.FindActionMap("Hero", throwIfNotFound: true);
        m_Hero_Movement = m_Hero.FindAction("Movement", throwIfNotFound: true);
        m_Hero_BasicAttack = m_Hero.FindAction("BasicAttack", throwIfNotFound: true);
        m_Hero_Cleave = m_Hero.FindAction("Cleave", throwIfNotFound: true);
        m_Hero_Dash = m_Hero.FindAction("Dash", throwIfNotFound: true);
        // Monster
        m_Monster = asset.FindActionMap("Monster", throwIfNotFound: true);
        m_Monster_Movement = m_Monster.FindAction("Movement", throwIfNotFound: true);
        m_Monster_BasicAttack = m_Monster.FindAction("BasicAttack", throwIfNotFound: true);
        m_Monster_Fireball = m_Monster.FindAction("Fireball", throwIfNotFound: true);
        m_Monster_Explosion = m_Monster.FindAction("Explosion", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Hero
    private readonly InputActionMap m_Hero;
    private IHeroActions m_HeroActionsCallbackInterface;
    private readonly InputAction m_Hero_Movement;
    private readonly InputAction m_Hero_BasicAttack;
    private readonly InputAction m_Hero_Cleave;
    private readonly InputAction m_Hero_Dash;
    public struct HeroActions
    {
        private InputMaster m_Wrapper;
        public HeroActions(InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_Hero_Movement;
        public InputAction @BasicAttack => m_Wrapper.m_Hero_BasicAttack;
        public InputAction @Cleave => m_Wrapper.m_Hero_Cleave;
        public InputAction @Dash => m_Wrapper.m_Hero_Dash;
        public InputActionMap Get() { return m_Wrapper.m_Hero; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(HeroActions set) { return set.Get(); }
        public void SetCallbacks(IHeroActions instance)
        {
            if (m_Wrapper.m_HeroActionsCallbackInterface != null)
            {
                Movement.started -= m_Wrapper.m_HeroActionsCallbackInterface.OnMovement;
                Movement.performed -= m_Wrapper.m_HeroActionsCallbackInterface.OnMovement;
                Movement.canceled -= m_Wrapper.m_HeroActionsCallbackInterface.OnMovement;
                BasicAttack.started -= m_Wrapper.m_HeroActionsCallbackInterface.OnBasicAttack;
                BasicAttack.performed -= m_Wrapper.m_HeroActionsCallbackInterface.OnBasicAttack;
                BasicAttack.canceled -= m_Wrapper.m_HeroActionsCallbackInterface.OnBasicAttack;
                Cleave.started -= m_Wrapper.m_HeroActionsCallbackInterface.OnCleave;
                Cleave.performed -= m_Wrapper.m_HeroActionsCallbackInterface.OnCleave;
                Cleave.canceled -= m_Wrapper.m_HeroActionsCallbackInterface.OnCleave;
                Dash.started -= m_Wrapper.m_HeroActionsCallbackInterface.OnDash;
                Dash.performed -= m_Wrapper.m_HeroActionsCallbackInterface.OnDash;
                Dash.canceled -= m_Wrapper.m_HeroActionsCallbackInterface.OnDash;
            }
            m_Wrapper.m_HeroActionsCallbackInterface = instance;
            if (instance != null)
            {
                Movement.started += instance.OnMovement;
                Movement.performed += instance.OnMovement;
                Movement.canceled += instance.OnMovement;
                BasicAttack.started += instance.OnBasicAttack;
                BasicAttack.performed += instance.OnBasicAttack;
                BasicAttack.canceled += instance.OnBasicAttack;
                Cleave.started += instance.OnCleave;
                Cleave.performed += instance.OnCleave;
                Cleave.canceled += instance.OnCleave;
                Dash.started += instance.OnDash;
                Dash.performed += instance.OnDash;
                Dash.canceled += instance.OnDash;
            }
        }
    }
    public HeroActions @Hero => new HeroActions(this);

    // Monster
    private readonly InputActionMap m_Monster;
    private IMonsterActions m_MonsterActionsCallbackInterface;
    private readonly InputAction m_Monster_Movement;
    private readonly InputAction m_Monster_BasicAttack;
    private readonly InputAction m_Monster_Fireball;
    private readonly InputAction m_Monster_Explosion;
    public struct MonsterActions
    {
        private InputMaster m_Wrapper;
        public MonsterActions(InputMaster wrapper) { m_Wrapper = wrapper; }
        public InputAction @Movement => m_Wrapper.m_Monster_Movement;
        public InputAction @BasicAttack => m_Wrapper.m_Monster_BasicAttack;
        public InputAction @Fireball => m_Wrapper.m_Monster_Fireball;
        public InputAction @Explosion => m_Wrapper.m_Monster_Explosion;
        public InputActionMap Get() { return m_Wrapper.m_Monster; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MonsterActions set) { return set.Get(); }
        public void SetCallbacks(IMonsterActions instance)
        {
            if (m_Wrapper.m_MonsterActionsCallbackInterface != null)
            {
                Movement.started -= m_Wrapper.m_MonsterActionsCallbackInterface.OnMovement;
                Movement.performed -= m_Wrapper.m_MonsterActionsCallbackInterface.OnMovement;
                Movement.canceled -= m_Wrapper.m_MonsterActionsCallbackInterface.OnMovement;
                BasicAttack.started -= m_Wrapper.m_MonsterActionsCallbackInterface.OnBasicAttack;
                BasicAttack.performed -= m_Wrapper.m_MonsterActionsCallbackInterface.OnBasicAttack;
                BasicAttack.canceled -= m_Wrapper.m_MonsterActionsCallbackInterface.OnBasicAttack;
                Fireball.started -= m_Wrapper.m_MonsterActionsCallbackInterface.OnFireball;
                Fireball.performed -= m_Wrapper.m_MonsterActionsCallbackInterface.OnFireball;
                Fireball.canceled -= m_Wrapper.m_MonsterActionsCallbackInterface.OnFireball;
                Explosion.started -= m_Wrapper.m_MonsterActionsCallbackInterface.OnExplosion;
                Explosion.performed -= m_Wrapper.m_MonsterActionsCallbackInterface.OnExplosion;
                Explosion.canceled -= m_Wrapper.m_MonsterActionsCallbackInterface.OnExplosion;
            }
            m_Wrapper.m_MonsterActionsCallbackInterface = instance;
            if (instance != null)
            {
                Movement.started += instance.OnMovement;
                Movement.performed += instance.OnMovement;
                Movement.canceled += instance.OnMovement;
                BasicAttack.started += instance.OnBasicAttack;
                BasicAttack.performed += instance.OnBasicAttack;
                BasicAttack.canceled += instance.OnBasicAttack;
                Fireball.started += instance.OnFireball;
                Fireball.performed += instance.OnFireball;
                Fireball.canceled += instance.OnFireball;
                Explosion.started += instance.OnExplosion;
                Explosion.performed += instance.OnExplosion;
                Explosion.canceled += instance.OnExplosion;
            }
        }
    }
    public MonsterActions @Monster => new MonsterActions(this);
    private int m_KeyboardSchemeIndex = -1;
    public InputControlScheme KeyboardScheme
    {
        get
        {
            if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.FindControlSchemeIndex("Keyboard");
            return asset.controlSchemes[m_KeyboardSchemeIndex];
        }
    }
    public interface IHeroActions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnBasicAttack(InputAction.CallbackContext context);
        void OnCleave(InputAction.CallbackContext context);
        void OnDash(InputAction.CallbackContext context);
    }
    public interface IMonsterActions
    {
        void OnMovement(InputAction.CallbackContext context);
        void OnBasicAttack(InputAction.CallbackContext context);
        void OnFireball(InputAction.CallbackContext context);
        void OnExplosion(InputAction.CallbackContext context);
    }
}
