﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;

public class Hero : Player
{
    [SerializeField] private int m_BasicAttackRange = 1;
    [SerializeField] private int m_BlastCooldown = 8;
    [SerializeField] private int m_DashRange = 4;
    [SerializeField] private int m_DashCooldown = 5;

    private InputMaster _controls;

    private bool _canUseBlast = true;
    private bool _canUseDash = true;

    public override void Awake()
    {
        base.Awake();
        _controls = new InputMaster();
        _isHero = true;
        _direction = Vector2Int.up;
    }

    private void Start()
    {
        _audioManager.Init(_isHero, GetComponent<AudioSource>());

    }

    private void OnEnable()
    {
        _controls.Hero.Movement.performed += Movement;
        _controls.Hero.BasicAttack.performed += BasicAttack;
        _controls.Hero.Cleave.performed += Blast;
        _controls.Hero.Dash.performed += Dash;
        _controls.Enable();
        SubscribeForGameEnd(OnGameEnd);
    }

    private void OnDisable()
    {
        _controls.Hero.Movement.performed -= Movement;
        _controls.Hero.BasicAttack.performed -= BasicAttack;
        _controls.Hero.Cleave.performed -= Blast;
        _controls.Hero.Dash.performed -= Dash;
        _controls.Disable();
        UnsubscribeForGameEnd(OnGameEnd);

    }

    private void Movement(InputAction.CallbackContext context)
    {
        Vector2 dir = context.ReadValue<Vector2>();
        Vector2Int direction = new Vector2Int((int)dir.x, (int)dir.y);
        Move(direction, 1, state =>
        {
            if (state == Status.started)
                animator.SetTrigger(Constants.MOVE_ANIMATION_TRIGGER);
        });
    }

    private void BasicAttack(InputAction.CallbackContext context)
    {
        Destroy(DirectionOfAttack.Forward, m_BasicAttackRange, 0f, status =>
        {
            if (status == Status.started)
                animator.SetTrigger(Constants.ATTACK_ANIMATION_TRIGGER);
        });
    }

    private void Dash(InputAction.CallbackContext obj)
    {
        if (_canUseDash)
            Move(_direction, m_DashRange, OnDashCallback);
    }

    private void OnDashCallback(Status state)
    {
        if (state == Status.started)
        {
            _canUseDash = false;
            DashAnimation();

            StartCoroutine(CooldownRoutine(m_DashCooldown, (isDone, elapsedTime) =>
            {
                float progress = 1 - elapsedTime / m_DashCooldown;
                int cooldownTime = Mathf.RoundToInt(m_DashCooldown - elapsedTime);
                string time = cooldownTime == 0 ? "" : cooldownTime.ToString();

                _uiManager.SetLoadingForDash(progress, time);

                if (isDone)
                    _canUseDash = true;

            }));
        }
    }

    private void Blast(InputAction.CallbackContext obj)
    {
        if (_canUseBlast)
            Destroy(DirectionOfAttack.AdjacentThreeSide, m_BasicAttackRange, 0f, OnDestroyCallback);
    }

    private void OnDestroyCallback(Status status)
    {
        if (status != Status.started)
            return;

        _canUseBlast = false;
        BlastAnimation();

        StartCoroutine(CooldownRoutine(m_BlastCooldown, (isDone, elapsedTime) =>
        {
            float progress = 1 - elapsedTime / m_BlastCooldown;
            int cooldownTime = Mathf.RoundToInt(m_BlastCooldown - elapsedTime);
            string time = cooldownTime == 0 ? "" : cooldownTime.ToString();

            _uiManager.SetLoadingForBlast(progress, time);

            if (isDone)
                _canUseBlast = true;

        }));
    }

    private void BlastAnimation()
    {
        animator.SetTrigger(Constants.ATTACK_ANIMATION_TRIGGER);
        transform.GetChild(1).gameObject.SetActive(true);
        _audioManager.Blast();
        Invoke("DeactivateBlast", 0.25f);
    }
    private void DashAnimation()
    {
        animator.SetTrigger(Constants.MOVE_ANIMATION_TRIGGER);
        transform.GetChild(2).gameObject.SetActive(true);
        _audioManager.Dash();
        Invoke("DeactivateDash", 1f);
    }


    private void DeactivateBlast()
    {
        transform.GetChild(1).gameObject.SetActive(false);
    }

    private void DeactivateDash()
    {
        transform.GetChild(2).gameObject.SetActive(false);
    }
}
