﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class Monster : Player
{
    [SerializeField] private int m_BasicAttackRange = 1;
    [SerializeField] private int m_FireballRange = 4;
    [SerializeField] private int m_FireballCooldown = 10;
    [SerializeField] private int m_ExplosionRange = 2;
    [SerializeField] private int m_ExplosionCooldown = 15;
    [SerializeField] private bool m_IsBot = false;

    private InputMaster _controls;
    private bool _canUseFireball = true;
    private bool _canUseExplosion = true;


    public override void Awake()
    {
        base.Awake();
        _controls = new InputMaster();
        _isHero = false;
        transform.rotation = Quaternion.Euler(0, 180, 0);
        _direction = Vector2Int.down;
        m_IsBot = PlayerPrefs.GetInt(Constants.PLAYERS) == 1 ? true : false;
    }

    private void Start()
    {
        _audioManager.Init(_isHero, GetComponent<AudioSource>());
        if  (m_IsBot)
            InvokeAutomate(1f);
    }
    private void OnEnable()
    {
        SubscribeForGameEnd(OnGameEnd);

        if (m_IsBot)
            return;

        _controls.Monster.Movement.performed += Movement;
        _controls.Monster.BasicAttack.performed += BasicAttack;
        _controls.Monster.Fireball.performed += Fireball;
        _controls.Monster.Explosion.performed += Explosion;
        _controls.Enable();
    }

    private void OnDisable()
    {
        UnsubscribeForGameEnd(OnGameEnd);

        if (m_IsBot)
            return;

        _controls.Monster.Movement.performed -= Movement;
        _controls.Monster.BasicAttack.performed -= BasicAttack;
        _controls.Monster.Fireball.performed -= Fireball;
        _controls.Monster.Explosion.performed -= Explosion;
        _controls.Disable();
    }

    private void Movement(InputAction.CallbackContext context)
    {
        Vector2 dir = context.ReadValue<Vector2>();
        Vector2Int direction = new Vector2Int((int)dir.x, (int)dir.y);
        Move(direction, 1, state =>
        {
            if (state == Status.started)
                animator.SetTrigger(Constants.MOVE_ANIMATION_TRIGGER);
            
        });
    }

    private void BasicAttack(InputAction.CallbackContext context)
    {
        Destroy(DirectionOfAttack.Forward, m_BasicAttackRange, 0f, status =>
        {
            if (status == Status.started)
                animator.SetTrigger(Constants.ATTACK_ANIMATION_TRIGGER);
            
        });
    }
    private void Fireball(InputAction.CallbackContext obj)
    {
        if (_canUseFireball)
            Destroy(DirectionOfAttack.Forward, m_FireballRange, 1f, FireballCallback);

    }

    private void FireballCallback(Status status)
    {
        if (status != Status.started)
            return;

        _canUseFireball = false;
        FireballAnimation();

        StartCoroutine(CooldownRoutine(m_FireballCooldown, (isDone, elapsedTime) =>
        {
            float progress = 1 - elapsedTime / m_FireballCooldown;
            int cooldownTime = Mathf.RoundToInt(m_FireballCooldown - elapsedTime);
            string time = cooldownTime == 0 ? "" : cooldownTime.ToString();

            _uiManager.SetLoadingForFireball(progress, time);

            if (isDone)
                _canUseFireball = true;

        }));
    }

    private void Explosion(InputAction.CallbackContext obj)
    {
        if (_canUseExplosion)
            Destroy(DirectionOfAttack.All, m_ExplosionRange, 1f, ExplosionCallback);
    }

    private void ExplosionCallback(Status status)
    {
        if (status != Status.started)
            return;

        _canUseExplosion = false;
        ExplosionAnimation();

        StartCoroutine(CooldownRoutine(m_ExplosionCooldown, (isDone, elapsedTime) =>
        {
            float progress = 1 - elapsedTime / m_ExplosionCooldown;
            int cooldownTime = Mathf.RoundToInt(m_ExplosionCooldown - elapsedTime);
            string time = cooldownTime == 0 ? "" : cooldownTime.ToString();

            _uiManager.SetLoadingForExplosion(progress, time);

            if (isDone)
                _canUseExplosion = true;

        }));
    }

    private void FireballAnimation()
    {
        animator.SetTrigger(Constants.EXPLOSION_ANIMATION_TRIGGER);
        transform.GetChild(2).gameObject.SetActive(true);
        _audioManager.Fireball();
        Invoke("DeactivateFireball", 1f);
    }

    private void ExplosionAnimation()
    {
        animator.SetTrigger(Constants.EXPLOSION_ANIMATION_TRIGGER);
        transform.GetChild(1).gameObject.SetActive(true);
        _audioManager.Explosion();
        Invoke("DeactivateExplosion", 1f);
    }
    private void DeactivateFireball()
    {
        transform.GetChild(2).gameObject.SetActive(false);
    }

    private void DeactivateExplosion()
    {
        transform.GetChild(1).gameObject.SetActive(false);
    }

    //Converts monster into a artificial intelligence
    private void Automate()
    {
        Vector2Int direction = _worldManager.GetFromToDirection(!_isHero);
        Move(direction, 1, OnMoveCallback);
    }

    private void InvokeAutomate(float time)
    {
        Invoke("Automate", time);
    }

    private void OnMoveCallback(Status status)
    {
        //Check if move completed
        if (status == Status.done)
        {
            if(_worldManager.CanDestroyHero() && _canUseExplosion)
            {
                AutomateExplosion();
                return;
            }

            if(_worldManager.CanDestroyHero(_direction, m_FireballRange) && _canUseFireball)
            {
                AutomateFireball();
                return;
            }


            //Whether player moved or not
            if (_worldManager.DidMove(_isHero))
                InvokeAutomate(0.25f);
            else
            {
                int i = 1;

                if (_canUseFireball && _worldManager.CanDestroyMoreThan(2, _direction, m_FireballRange))
                    i = 2;

                if (_canUseExplosion && _worldManager.CanDestroyMoreThan(3))
                    i = 3;

                int randomAction = UnityEngine.Random.Range(0, i);

                switch (randomAction)
                {
                    case 0:
                        //Destroy the obstacle
                        AutomateBasicAttack();
                        break;
                    case 1:
                        AutomateFireball();
                        break;
                    case 2:
                        AutomateExplosion();
                        break;
                    default:
                        break;
                }
            }
        }
        else if (status == Status.cancelled)
        {
            //wait for other action to end
            InvokeAutomate(1f);
        }
        else if (status == Status.started)
        {
            //Play move animation
            animator.SetTrigger(Constants.MOVE_ANIMATION_TRIGGER);
        }

    }

    private void AutomateBasicAttack()
    {
        Destroy(DirectionOfAttack.Forward, m_BasicAttackRange, 0f, basicAttackStatus =>
        {
            if (basicAttackStatus == Status.started)
                animator.SetTrigger(Constants.ATTACK_ANIMATION_TRIGGER);
            else if (basicAttackStatus == Status.done)
                InvokeAutomate(0.25f);
        });
    }

    private void AutomateFireball()
    {
        Destroy(DirectionOfAttack.Forward, m_FireballRange, 1f, fireballStatus =>
        {
            if (fireballStatus == Status.started)
                FireballCallback(fireballStatus);
            else if (fireballStatus == Status.done)
                InvokeAutomate(0.25f);
        });
    }

    private void AutomateExplosion()
    {
        Destroy(DirectionOfAttack.All, m_ExplosionRange, 1f, explosionStatus =>
        {
            if (explosionStatus == Status.started)
                ExplosionCallback(explosionStatus);
            else if (explosionStatus == Status.done)
                InvokeAutomate(0.25f);
        });
    }

}
