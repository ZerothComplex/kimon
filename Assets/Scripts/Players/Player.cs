﻿using UnityEngine;
using System.Collections;
using System;

public class Player : MonoBehaviour
{
    private enum State
    {
        idle,
        moving,
        attacking,
        dead,
    }

    public enum Status
    {
        cancelled,
        started,
        done
    }

    public enum DirectionOfAttack
    {
        Backward,
        Forward,
        AdjacentThreeSide, //Forward and adjacent two side
        All
    }

    public float speed;
    public float moveTime;
    public float rotateTime;
    public Animator animator;

    protected UIManager _uiManager;
    protected Sekai.WorldManager _worldManager;
    protected AudioManager _audioManager;
    protected Vector2Int _direction;
    protected bool _isHero;

    private State _state = State.idle;

    public static bool isPaused = false;

    public virtual void Awake()
    {
        _worldManager = FindObjectOfType<Sekai.WorldManager>();
        _uiManager = FindObjectOfType<UIManager>();
        _audioManager = FindObjectOfType<AudioManager>();
    }

    public void Move(Vector2Int direction, int range, Action<Status> callback = null)
    {
        if (isPaused || _state != State.idle)
        {
            callback?.Invoke(Status.cancelled);
            return;
        }

        _state = State.moving;
        callback?.Invoke(Status.started);

        _direction = direction;

        Vector3 directionXZ = new Vector3(_direction.x, 0, _direction.y);
        Vector3 newPos = transform.position;

        for (int i = 1; i <= range; i++)
        {
            if (_worldManager.CanMoveTo(_direction, _isHero))
            {
                newPos += directionXZ;
                _worldManager.MoveTo(_direction, _isHero);
            }
        }

        Quaternion newRot = Quaternion.LookRotation(directionXZ);
        StartCoroutine(Move(transform.position, newPos, transform.rotation, newRot, moveTime, callback));
    }

    public void Destroy(DirectionOfAttack directionOfAttack, int range, float duration, Action<Status> callback = null)
    {
        if (isPaused || _state != State.idle)
        {
            callback?.Invoke(Status.cancelled);
            return;
        }

        _state = State.attacking;
        callback?.Invoke(Status.started);
        StartCoroutine(WaitForDestroy(duration, directionOfAttack, range, callback));
    }

    public IEnumerator CooldownRoutine(float time, Action<bool, float> onComplete)
    {
        float elapsedTime = 0f;
        while (elapsedTime < time)
        {
            yield return null;
            onComplete?.Invoke(false, elapsedTime);
            elapsedTime += Time.smoothDeltaTime;
        }

        elapsedTime = time;
        onComplete?.Invoke(true, elapsedTime);
    }

    public void SubscribeForGameEnd(Action<bool> callback)
    {
        _worldManager.onBattleEnded += callback;

    }

    public void UnsubscribeForGameEnd(Action<bool> callback)
    {
        _worldManager.onBattleEnded -= callback;
    }


    private IEnumerator Move(Vector3 startPos, Vector3 endPos, Quaternion startRot, Quaternion endRot, float time, Action<Status> callback = null)
    {
        float elapsedTime = 0f;
        while (elapsedTime < time)
        {
            yield return null;
            transform.position = Vector3.Lerp(startPos, endPos, elapsedTime / time);
            transform.rotation = Quaternion.Lerp(startRot, endRot, elapsedTime / time);
            elapsedTime += Time.smoothDeltaTime;
        }

        transform.position = endPos;
        transform.rotation = endRot;

        _state = State.idle;

        callback?.Invoke(Status.done);
    }

    private IEnumerator WaitForDestroy(float waitTime, DirectionOfAttack directionOfAttack, int range, Action<Status> callback = null)
    {
        switch (directionOfAttack)
        {
            case DirectionOfAttack.Forward:
                _worldManager.DestroyCells(_isHero, _direction, range);
                break;
            case DirectionOfAttack.Backward:
                break;
            case DirectionOfAttack.AdjacentThreeSide:
                if (Mathf.Abs(_direction.x) > 0)
                {
                    _worldManager.DestroyCells(_isHero, _direction, range);
                    _worldManager.DestroyCells(_isHero, Vector2Int.up, range);
                    _worldManager.DestroyCells(_isHero, Vector2Int.down, range);
                    _worldManager.DestroyCells(_isHero, new Vector2Int(_direction.x, _direction.x), range);
                    _worldManager.DestroyCells(_isHero, new Vector2Int(_direction.x, -_direction.x), range);
                }
                else if (Mathf.Abs(_direction.y) > 0)
                {
                    _worldManager.DestroyCells(_isHero, _direction, range);
                    _worldManager.DestroyCells(_isHero, Vector2Int.left, range);
                    _worldManager.DestroyCells(_isHero, Vector2Int.right, range);
                    _worldManager.DestroyCells(_isHero, new Vector2Int(_direction.y, _direction.y), range);
                    _worldManager.DestroyCells(_isHero, new Vector2Int(-_direction.y, _direction.y), range);
                }

                break;
            case DirectionOfAttack.All:
                _worldManager.DestroyCells(_isHero, Vector2Int.up, range);
                _worldManager.DestroyCells(_isHero, Vector2Int.down, range);
                _worldManager.DestroyCells(_isHero, Vector2Int.left, range);
                _worldManager.DestroyCells(_isHero, Vector2Int.right, range);

                //Hack for diagonal cells
                _worldManager.DestroyCells(_isHero, new Vector2Int(1, 1), 1);
                _worldManager.DestroyCells(_isHero, new Vector2Int(-1, 1), 1);
                _worldManager.DestroyCells(_isHero, new Vector2Int(1, -1), 1);
                _worldManager.DestroyCells(_isHero, new Vector2Int(-1, -1), 1);
                break;
        }


        yield return new WaitForSeconds(waitTime);
        _state = State.idle;
        callback?.Invoke(Status.done);
    }

    public void OnGameEnd(bool didHeroWin)
    {
        if ((didHeroWin && _isHero) || (!didHeroWin && !_isHero))
            Win();
        else
            Die();

    }

    private void Win()
    {
        animator.SetTrigger(Constants.WIN_ANIMATION_TRIGGER);
    }

    private void Die()
    {
        _state = State.dead;
        animator.SetTrigger(Constants.DIE_ANIMATION_TRIGGER);
        transform.GetChild(3).gameObject.SetActive(true);
        Invoke("Disappear", 1f);
    }


    private void Disappear()
    {
        transform.GetChild(0).gameObject.SetActive(false);
    }

  
}
