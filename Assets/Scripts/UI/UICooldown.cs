﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UICooldown : MonoBehaviour
{
    [SerializeField] private Image m_Image;
    [SerializeField] private Text m_Text;

    public void Set(float progress, string time)
    {
        m_Image.fillAmount = progress;
        m_Text.text = time;
    }
}