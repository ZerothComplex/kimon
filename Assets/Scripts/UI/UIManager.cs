﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private UICooldown m_Blast;
    [SerializeField] private UICooldown m_Dash;
    [SerializeField] private UICooldown m_Fireball;
    [SerializeField] private UICooldown m_Explosion;

    public void SetLoadingForBlast(float progress, string time)
    {
        m_Blast.Set(progress, time);
    }

    public void SetLoadingForDash(float progress, string time)
    {
        m_Dash.Set(progress, time);
    }

    public void SetLoadingForFireball(float progress, string time)
    {
        m_Fireball.Set(progress, time);
    }

    public void SetLoadingForExplosion(float progress, string time)
    {
        m_Explosion.Set(progress, time);
    }

    public void ResetAll()
    {
        SetLoadingForBlast(0f,"");
        SetLoadingForDash(0f,"");
        SetLoadingForFireball(0f,"");
        SetLoadingForExplosion(0f,"");
    }

}
