﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
     private AudioSource m_HeroSource;
     private AudioSource m_MonsterSource;

    [SerializeField] private AudioSource m_UISource;
    [SerializeField] private AudioSource m_BackgroundSource;
    [SerializeField] private AudioClip m_Button;
    [SerializeField] private AudioClip m_Blast;
    [SerializeField] private AudioClip m_Dash;
    [SerializeField] private AudioClip m_Fireball;
    [SerializeField] private AudioClip m_Explosion;
    [SerializeField] private AudioClip m_EndGame;

    // Singleton instance.
    public static AudioManager Instance = null;

    // Initialize the singleton instance.
     void Awake()
    {
        // If there is not already an instance of SoundManager, set it to this.
        if (Instance == null)
            Instance = this;
        //If an instance already exists, destroy whatever this object is to enforce the singleton.
        else if (Instance != this)
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }


    public void Init(bool isHero, AudioSource source)
    {
        if (isHero)
            m_HeroSource = source;
        else
            m_MonsterSource = source;
    }

    public void Button()
    {
        m_UISource.PlayOneShot(m_Button);
    }

    public void Blast()
    {
        m_HeroSource.PlayOneShot(m_Blast);
    }
    public void Dash()
    {
        m_HeroSource.PlayOneShot(m_Dash);
    }

    public void Fireball()
    {
        m_MonsterSource.PlayOneShot(m_Fireball);
    }
    public void Explosion()
    {
        m_MonsterSource.PlayOneShot(m_Explosion);
    }

    public void EndGame(bool isHero)
    {
        PlayClip(isHero, m_EndGame);
    }

    private void Start()
    {
        m_UISource = GetComponentInChildren<AudioSource>();
        m_BackgroundSource.Play();
    }

    private void PlayClip(bool isHero, AudioClip clip)
    {
        if (clip == null)
            return;

        if (isHero)
            m_HeroSource.PlayOneShot(clip);
        else
            m_MonsterSource.PlayOneShot(clip);
    }
}
