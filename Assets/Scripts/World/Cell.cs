﻿using UnityEngine;
using System.Collections;

namespace Sekai
{
    [System.Serializable]
    public class Cell
    {
        public enum Type
        {
            Empty,
            Destructable,
            Indestructable,
            Hero,
            Monster,
            Gate,
            Wall
        }

        public Vector3 pos;
        public Type type;
        public GameObject obj;

        public Cell(Type type)
        {
            this.type = type;
        }

        public void Destroy(bool destroyGate = false, float delay = 0f)
        {
            if (type == Type.Gate && !destroyGate)
            {
                obj.GetComponent<MeshRenderer>().enabled = false;
                obj.transform.GetChild(0).gameObject.SetActive(false);
                obj.transform.GetChild(1).gameObject.SetActive(true);
                obj.transform.GetChild(2).gameObject.SetActive(true);
            }
            else
            {
                obj.transform.GetChild(0).gameObject.SetActive(false);
                obj.transform.GetChild(1).gameObject.SetActive(true);
                Object.Destroy(obj, 1f);
                obj.GetComponent<AudioSource>().Play();
            }
        }
    }
}
