﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Sekai
{
    [System.Serializable]
    public class World
    {
        [Range(5, 30)]
        public int columns = 21;
        [Range(5, 30)]
        public int rows = 17;
        public bool generateDestructable = true;
        public int maxDestructable = 100;
        public int minDestructable = 85;
        //public bool isMonsterBot = false;
        //public bool isHeroBot = false;

        private Vector2Int _lastHeroIndex;
        private Vector2Int _heroIndex;
        private Vector2Int _lastMonsterIndex;
        private Vector2Int _monsterIndex;
        private Vector2Int _gateIndex;
        private bool _isGateDestroyed = false;

        public bool IsGateDestroyed { get => _isGateDestroyed; }
        public System.Action OnGateDestroyed;
        public Cell[,] matrix;


        /// <summary>
        /// Generate all the cells in world
        /// </summary>
        public void Generate()
        {
            bool isGate = false;
            bool isHeroSpawn = false;
            bool isMonsterSpawn = false;
            int count = 0;
            _isGateDestroyed = false;

            if (!generateDestructable)
                count = maxDestructable;

            matrix = new Cell[columns, rows];

            for (int i = 0; i < columns; i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    Cell.Type type = Cell.Type.Empty;

                    if (i == 0 || j == 0 || i == columns - 1 || j == rows - 1) //All border cells
                        type = Cell.Type.Wall;
                    else if (i % 2 == 0 && j % 2 == 0) //All even cells
                        type = Cell.Type.Indestructable;
                    else
                    {

                        if (Random.value < 0.5f)
                        {
                            if (!isGate
                               && i > columns / 2
                               && Random.value < 0.2f)
                            {
                                isGate = true;
                                _gateIndex = new Vector2Int(i, j);
                                type = Cell.Type.Gate;
                            }
                            else if (count < maxDestructable)
                            {
                                count++;
                                type = Cell.Type.Destructable;
                            }

                        }
                        else
                        {
                            if (!isHeroSpawn && j < rows / 2
                                  && (i < 4)
                                  && Random.value < 0.8f)
                            {
                                isHeroSpawn = true;
                                _heroIndex = new Vector2Int(i, j);
                                type = Cell.Type.Hero;

                            }
                            else if (!isMonsterSpawn && j > rows / 2
                                && (i > columns - 4)
                                && Random.value > 0.4f)
                            {
                                isMonsterSpawn = true;
                                _monsterIndex = new Vector2Int(i, j);
                                type = Cell.Type.Monster;
                            }
                        }

                    }

                    Cell newCell = new Cell(type)
                    {
                        pos = new Vector3(-columns / 2 + i, 0, -rows / 2 + j)
                    };
                    matrix[i, j] = newCell;
                }
            }

            int number = Random.Range(1, 5);


            if (!isMonsterSpawn || !isHeroSpawn || !isGate || count < minDestructable)
                Generate();

        }

        public bool ReachedGate(bool isHero)
        {
            return isHero ? _heroIndex == _gateIndex : _monsterIndex == _gateIndex;
        }


        public bool DestroyCells(bool isHero, Vector2Int direction, int number)
        {
            Vector2Int index = isHero ? _heroIndex : _monsterIndex;
            bool isOtherPlayerDestroyed = false;

            for (int i = 1; i <= number; i++)
            {
                Vector2Int cellIndex = index + new Vector2Int(direction.x * i, direction.y * i);

                if (cellIndex.x < 0
                    || cellIndex.y < 0
                    || cellIndex.x >= columns
                    || cellIndex.y >= rows)
                    continue;

                isOtherPlayerDestroyed |= (cellIndex == _heroIndex || cellIndex == _monsterIndex);

                if (matrix[cellIndex.x, cellIndex.y].type == Cell.Type.Destructable ||
                    matrix[cellIndex.x, cellIndex.y].type == Cell.Type.Gate)
                    DestroyCell(isHero, cellIndex, 0);
            }

            return isOtherPlayerDestroyed;
        }

        public bool CanMoveTo(Vector2Int direction, bool isHero)
        {
            Vector2Int index = isHero ? _heroIndex : _monsterIndex;
            Vector2Int nextCellIndex = index + direction;
            //Debug.LogFormat("Can move: {0}, \nindex:{1}. \ndirection: {2}\ncell type: {3}", matrix[nextCellIndex.x, nextCellIndex.y].type == Cell.Type.Empty, nextCellIndex, direction, matrix[nextCellIndex.x, nextCellIndex.y].type);
            if (isHero)
                _lastHeroIndex = _heroIndex;
            else
                _lastMonsterIndex = _monsterIndex;
            return matrix[nextCellIndex.x, nextCellIndex.y].type == Cell.Type.Empty;
        }

        public void MoveTo(Vector2Int direction, bool isHero)
        {
            Vector2Int index = isHero ? _heroIndex : _monsterIndex;
            Vector2Int nextCellIndex = index + direction;
            matrix[index.x, index.y].type = Cell.Type.Empty;
            matrix[nextCellIndex.x, nextCellIndex.y].type = isHero ? Cell.Type.Hero : Cell.Type.Monster;
            if (isHero)
                _heroIndex = nextCellIndex;
            else
                _monsterIndex = nextCellIndex;
        }

        public bool DidMove(bool isHero)
        {
            return isHero ? _lastHeroIndex != _heroIndex : _lastMonsterIndex != _monsterIndex;
        }


        public Vector2Int GetFromToDirection(bool isFromMonsterToHero)
        {
            int x = _monsterIndex.x > _heroIndex.x ? -1 : 1;
            int y = _monsterIndex.y > _heroIndex.y ? -1 : 1;

            if (isFromMonsterToHero)
            {
                if (matrix[_monsterIndex.x + x, _monsterIndex.y].type == Cell.Type.Indestructable)
                    x = 0;
                if (matrix[_monsterIndex.x, _monsterIndex.y + y].type == Cell.Type.Indestructable)
                    y = 0;
            }
            else
            {
                if (matrix[_heroIndex.x + x, _heroIndex.y].type == Cell.Type.Indestructable)
                    x = 0;
                if (matrix[_heroIndex.x, _heroIndex.y + y].type == Cell.Type.Indestructable)
                    y = 0;
            }

            y = _monsterIndex.y == _heroIndex.y && x != 0 ? 0 : y;
            x = _monsterIndex.x == _heroIndex.x && y != 0 ? 0 : x;

            if (Mathf.Abs(x) == 1 && Mathf.Abs(y) == 1)
            {
                if (Random.value > 0.5)
                    y = 0;
                else
                    x = 0;
            }

            return isFromMonsterToHero ? new Vector2Int(x, y) : new Vector2Int(-x, -y);
        }

        public bool CanDestroyMoreThan(int minDestructableCount)
        {
            int count = 0;

            if (IsDestructable(matrix[_monsterIndex.x + 1, _monsterIndex.y + 1].type))
                count++;
            if (IsDestructable(matrix[_monsterIndex.x + 1, _monsterIndex.y].type))
                count++;
            if (IsDestructable(matrix[_monsterIndex.x + 1, _monsterIndex.y - 1].type))
                count++;
            if (IsDestructable(matrix[_monsterIndex.x - 1, _monsterIndex.y + 1].type))
                count++;
            if (IsDestructable(matrix[_monsterIndex.x - 1, _monsterIndex.y].type))
                count++;
            if (IsDestructable(matrix[_monsterIndex.x - 1, _monsterIndex.y - 1].type))
                count++;

            if (count >= minDestructableCount)
                return true;

            return false;
        }

        public bool CanDestroyMoreThan(int minDestructableCount, Vector2Int direction, int range)
        {
            int count = 0;

            for (int i = 1; i <= range; i++)
            {
                Vector2Int index = _monsterIndex + (direction * i);

                if (IsDestructable(matrix[index.x, index.y].type))
                    count++;
            }

            if (count >= minDestructableCount)
                return true;

            return false;
        }

        public bool CanDestroyHero()
        {
            if (IsHero(matrix[_monsterIndex.x + 1, _monsterIndex.y + 1].type))
                return true;
            if (IsHero(matrix[_monsterIndex.x + 1, _monsterIndex.y].type))
                return true;
            if (IsHero(matrix[_monsterIndex.x + 1, _monsterIndex.y - 1].type))
                return true;
            if (IsHero(matrix[_monsterIndex.x - 1, _monsterIndex.y + 1].type))
                return true;
            if (IsHero(matrix[_monsterIndex.x - 1, _monsterIndex.y].type))
                return true;
            if (IsHero(matrix[_monsterIndex.x - 1, _monsterIndex.y - 1].type))
                return true;

            return false;
        }

        public bool CanDestroyHero(Vector2Int direction, int range)
        {
            for (int i = 1; i <= range; i++)
            {
                Vector2Int index = _monsterIndex + (direction * i);

                if (index.x < columns && index.x >= 0 && index.y < rows && index.y >= 0)
                {
                    if (IsHero(matrix[index.x, index.y].type))
                        return true;
                }
            }

            return false;
        }

        private bool IsDestructable(Cell.Type type)
        {
            if (type == Cell.Type.Destructable || type == Cell.Type.Hero)
                return true;

            return false;
        }

        private bool IsHero(Cell.Type type)
        {
            if (type == Cell.Type.Hero)
                return true;

            return false;
        }


        private void DestroyCell(bool isHero, Vector2Int index, float delay)
        {
            if (index == _gateIndex)
            {
                if (_isGateDestroyed)
                    return;

                _isGateDestroyed = true;

                if (!isHero)
                {
                    OnGateDestroyed?.Invoke();
                    _gateIndex = Vector2Int.zero;
                }
            }

            matrix[index.x, index.y].Destroy(!isHero, delay);
            matrix[index.x, index.y].type = Cell.Type.Empty;
        }
    }
}
