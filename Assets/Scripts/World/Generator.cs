using System.Collections.Generic;
using UnityEngine;

namespace Sekai
{
    public class Generator : MonoBehaviour
    {
        [SerializeField] private Transform parent;
        [SerializeField] private GameObject wallXCellPrefab;
        [SerializeField] private GameObject wallYCellPrefab;
        [SerializeField] private GameObject wallCornerCellPrefab;
        [SerializeField] private GameObject destructibleCellPrefab;
        [SerializeField] private GameObject gatePrefab;
        [SerializeField] private GameObject indestructibleCellPrefab;
        [SerializeField] private GameObject emptyDarkPrefab;
        [SerializeField] private GameObject emptyLightPrefab;
        [SerializeField] private GameObject heroPrefab;
        [SerializeField] private GameObject monsterPrefab;

        private bool isDark = true;

        /// <summary>
        /// Generates new map data and instatiates all the cells
        /// </summary>
        public void Generate(ref World world)
        {
            DestroyChilds();
            world.Generate();
            Spawn(world);
        }

        public void Spawn(World world)
        {
            Transform floorParent = new GameObject().transform;
            floorParent.name = "Floor";
            floorParent.transform.SetParent(parent);

            Transform cellParent = floorParent.transform;

            for (int i = 0; i < world.columns; i++)
            {
                for (int j = 0; j < world.rows; j++)
                {
                    GameObject prefab = null;
                    switch (world.matrix[i, j].type)
                    {
                        case Cell.Type.Wall:
                            if ((i == 0 && j == 0) ||
                                (i == 0 && j == world.rows - 1) ||
                                (i == world.columns - 1 && j == 0) ||
                                (i == world.columns - 1 && j == world.rows - 1))
                                prefab = wallCornerCellPrefab;
                            else if (i == 0 || i == world.columns - 1)
                                prefab = wallXCellPrefab;
                            else if (j == 0 || j == world.rows - 1)
                                prefab = wallYCellPrefab;
                            break;
                        case Cell.Type.Destructable:
                            prefab = destructibleCellPrefab;
                            break;
                        case Cell.Type.Indestructable:
                            prefab = indestructibleCellPrefab;
                            break;
                        case Cell.Type.Hero:
                            prefab = heroPrefab;
                            break;
                        case Cell.Type.Monster:
                            prefab = monsterPrefab;
                            break;
                        case Cell.Type.Gate:
                            prefab = gatePrefab;
                            break;
                    }

                    if (world.matrix[i, j].type == Cell.Type.Hero || world.matrix[i, j].type == Cell.Type.Monster)
                        cellParent = parent;
                    else
                        cellParent = floorParent;

                    if (prefab != null)
                        world.matrix[i, j].obj = Instantiate(prefab, world.matrix[i, j].pos, Quaternion.identity, cellParent);

                }
            }

            for (int i = 1; i < world.columns - 1; i++)
            {
                for (int j = 1; j < world.rows - 1; j++)
                {
                    GameObject prefab = isDark ? emptyDarkPrefab : emptyLightPrefab;
                    GameObject floorObj = Instantiate(prefab, world.matrix[i, j].pos, Quaternion.identity, floorParent.transform);
                    isDark = !isDark;
                }
            }

            StaticBatchingUtility.Combine(floorParent.gameObject);
        }

        /// <summary>
        /// Destroys all cells gameobject and clears the list
        /// </summary>
        public void DestroyChilds()
        {
            List<Transform> childList = new List<Transform>();

            foreach (Transform cell in parent)
                childList.Add(cell);

            foreach (Transform cell in childList)
            {
                if (cell != null)
                    DestroyImmediate(cell.gameObject);
            }

            childList.Clear();
        }

    }

}
