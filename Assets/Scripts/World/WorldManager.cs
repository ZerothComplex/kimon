﻿using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections;

namespace Sekai
{
    public class WorldManager : MonoBehaviour
    {
        [SerializeField] private World m_World;
        [SerializeField] private GameObject m_EndGameUI;
        [SerializeField] private UnityEngine.UI.Text m_Score;
        [SerializeField] private UnityEngine.UI.Text m_Result;
        [SerializeField] private GameObject m_GateClosedStatus;
        [SerializeField] private Generator m_Generator;
        [SerializeField] private UIManager m_UIManager;

        public System.Action<bool> onBattleEnded;

        private int _heroWins = 0;
        private int _monsterWins = 0;

        [ContextMenu("Generate")]
        public void Generate()
        {
            m_Generator.Generate(ref m_World);
        }

        public void MoveTo(Vector2Int direction, bool isHero)
        {
            m_World.MoveTo(direction, isHero);
            if (m_World.ReachedGate(isHero))
                EndGame(isHero);
        }

        public bool CanMoveTo(Vector2Int direction, bool isHero)
        {
            return m_World.CanMoveTo(direction, isHero);
        }

        public bool DidMove(bool isHero)
        {
            return m_World.DidMove(isHero);
        }

        public void DestroyCells(bool isHero, Vector2Int direction, int number)
        {
            bool isPlayerDestroyed = m_World.DestroyCells(isHero, direction, number);

            if (isPlayerDestroyed)
            {
                if (!isHero)
                    EndGame(false);
                else if (m_World.IsGateDestroyed)
                    EndGame(true);
            }

        }

        public Vector2Int GetFromToDirection(bool isFromMonsterToHero)
        {
            return m_World.GetFromToDirection(isFromMonsterToHero);
        }

        public bool CanDestroyMoreThan(int minDestructableCount)
        {
            return m_World.CanDestroyMoreThan(minDestructableCount);
        }

        public bool CanDestroyMoreThan(int minDestructableCount, Vector2Int direction, int range)
        {
            return m_World.CanDestroyMoreThan(minDestructableCount, direction, range);
        }

        public bool CanDestroyHero()
        {
            return m_World.CanDestroyHero();
        }

        public bool CanDestroyHero(Vector2Int direction, int range)
        {
            return m_World.CanDestroyHero(direction, range);
        }

        public void EndGame(bool didHeroWin)
        {
            AudioManager.Instance.EndGame(didHeroWin);
            SetScoreAndVictoryText(didHeroWin);
            Player.isPaused = true;
            m_UIManager.ResetAll();
            onBattleEnded?.Invoke(didHeroWin);
            Invoke("Wait", 2f);
        }

        private void Wait()
        {
            m_EndGameUI.SetActive(true);
        }

        public void Reset()
        {
            m_GateClosedStatus.SetActive(false);
            Resume();
            AudioManager.Instance.Button();
            m_UIManager.ResetAll();
            Generate();
        }

        public void Back()
        {
            Time.timeScale = 1;
            Player.isPaused = false;
            AudioManager.Instance.Button();
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(0);
        }

        public void QuitGame()
        {
            AudioManager.Instance.Button();
            Application.Quit();
        }


        private void Start()
        {
            _heroWins = PlayerPrefs.GetInt(Constants.HERO_WINS, 0);
            _monsterWins = PlayerPrefs.GetInt(Constants.MONSTER_WINS, 0);
            m_World.OnGateDestroyed = () => m_GateClosedStatus.SetActive(true);
            SetScore();
            Generate();
        }

        private void Update()
        {
            Keyboard keyboard = InputSystem.GetDevice<Keyboard>();
            if (keyboard.escapeKey.wasPressedThisFrame)
            {
                if (m_EndGameUI.activeSelf)
                    Resume();
                else
                    Pause();
            }
        }

        private void SetScoreAndVictoryText(bool didHeroWin)
        {
            if (didHeroWin)
            {
                _heroWins++;
                m_Result.text = Constants.BLUE_DEMON_TEXT_COLOR + "Blue Demon" + Constants.TEXT_COLOR_CLOSE + " Wins";
            }
            else
            {
                _monsterWins++;
                m_Result.text = Constants.RED_DEMON_TEXT_COLOR + "Red Demon" + Constants.TEXT_COLOR_CLOSE + " Wins";
            }

            SetScore();

            PlayerPrefs.SetInt(Constants.HERO_WINS, _heroWins);
            PlayerPrefs.SetInt(Constants.MONSTER_WINS, _monsterWins);
        }

        private void SetScore()
        {
            m_Score.text = Constants.BLUE_DEMON_TEXT_COLOR + _heroWins + Constants.TEXT_COLOR_CLOSE +
                " : " +
                Constants.RED_DEMON_TEXT_COLOR + _monsterWins + Constants.TEXT_COLOR_CLOSE;
        }

        private void Pause()
        {
            m_EndGameUI.SetActive(true);
            m_Result.text = "Paused";
            Player.isPaused = true;
            Time.timeScale = 0;
        }

        private void Resume()
        {
            m_EndGameUI.SetActive(false);
            Player.isPaused = false;
            Time.timeScale = 1;
        }
    }
}
