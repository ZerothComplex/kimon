﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    [SerializeField] private GameObject m_Help;
    [SerializeField] private GameObject m_Loading;

    private AsyncOperation _asyncSceneLoad;

    public void OnePlayer()
    {
        PlayerPrefs.SetInt(Constants.PLAYERS, 1);
        StartGame();
    }

    public void TwoPlayer()
    {
        PlayerPrefs.SetInt(Constants.PLAYERS, 2);
        StartGame();
    }

    public void OpenHelp()
    {
        AudioManager.Instance.Button();
        m_Help.SetActive(true);
    }

    public void CloseHelp()
    {
        AudioManager.Instance.Button();
        m_Help.SetActive(false);
    }


    public void StartGame()
    {
        AudioManager.Instance.Button();
        m_Loading.SetActive(true);
        StartCoroutine(WaitForSceneLoading());
    }

    public void QuitGame()
    {
        AudioManager.Instance.Button();
        Application.Quit();
    }

    private void Start()
    {
        StartCoroutine(Wait());
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(1f);
        _asyncSceneLoad = SceneManager.LoadSceneAsync(1);
        _asyncSceneLoad.allowSceneActivation = false;
    }

    private IEnumerator WaitForSceneLoading()
    {
        yield return new WaitForSeconds(1f);
        yield return new WaitUntil(() => _asyncSceneLoad.progress >= 0.9f);
        _asyncSceneLoad.allowSceneActivation = true;
    }

  
}
