﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class PlayVideo : MonoBehaviour
{
    private RawImage _rawImage;

    private void Start()
    {
        VideoPlayer videoPlayer = GetComponent<VideoPlayer>();
        _rawImage = GetComponent<RawImage>();

        videoPlayer.started += OnVideoStarted;
        videoPlayer.prepareCompleted += OnVideoPrepared;
        videoPlayer.Prepare();
    }

    private void OnVideoStarted(VideoPlayer player)
    {
        _rawImage.color = Color.white;
    }

    private void OnDisable()
    {
        _rawImage.color = new Color(0.27f, 0.27f, 0.27f);
    }

    private void OnVideoPrepared(VideoPlayer player)
    {
        _rawImage.texture = player.texture;
        player.Play();
        player.started -= OnVideoPrepared;
    }
}
