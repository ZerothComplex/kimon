﻿public static class Constants
{
    public const string EXPLOSION_ANIMATION_TRIGGER = "Take Damage";
    public const string MOVE_ANIMATION_TRIGGER = "Run";
    public const string ATTACK_ANIMATION_TRIGGER = "Attack 01";
    public const string WIN_ANIMATION_TRIGGER = "Jump";
    public const string DIE_ANIMATION_TRIGGER = "Die";

    public const string HERO_WINS = "HeroWins";
    public const string MONSTER_WINS = "MonsterWins";

    public const string PLAYERS = "Players";

    public const string BLUE_DEMON_TEXT_COLOR = "<color=#415DBE>";
    public const string RED_DEMON_TEXT_COLOR = "<color=#E83F35>";
    public const string TEXT_COLOR_CLOSE = "</color>";
}
