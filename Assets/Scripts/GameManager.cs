﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Sekai;

public class GameManager : Singleton<GameManager>
{
    //Map
    [SerializeField]
    private WorldManager worldManager;

    //UI
    [SerializeField]
    private Text score;
    [SerializeField]
    public GameObject gameEndUI;
    [SerializeField]
    private Text result;



    void Start()
    {
        StartGame();
    }


    void StartGame()
    {
        //Generate new map
        worldManager.Generate();
    }

    public void EndGame(System.Action onRestart)
    {
        gameEndUI.SetActive(true);
    }
}
